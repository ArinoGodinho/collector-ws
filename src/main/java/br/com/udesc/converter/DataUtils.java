package br.com.udesc.converter;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;

import org.joda.time.DateTime;

/**
 * @author arino.godinho
 * 2017
 */
public class DataUtils {

	public static LocalDate toLocalDate(String data){
		String[] dt = data.split("/");
		LocalDate data_convertida = LocalDate.of(
				Integer.parseInt(dt[2]), 
				Integer.parseInt(dt[1]),
				Integer.parseInt(dt[0])
			);

		return data_convertida;
	}
	
	public static LocalDateTime toLocalDateTime(LocalDate data, String hora){
		String[] hr = hora.split(":");
		LocalDateTime hora_convertida = LocalDateTime.of(
				data.getYear(),
				data.getMonth(),
				data.getDayOfMonth(),
				Integer.parseInt(hr[0]), 
				Integer.parseInt(hr[1]),
				Integer.parseInt(hr[2])
			);

		return hora_convertida;
	}
	
	public static long dias(LocalDate dataInicial, LocalDate dataFinal){
		return  Duration.between(dataInicial.atStartOfDay(), dataFinal.atStartOfDay()).toDays();
	}
	
	public static DateTime toDateTime(LocalDateTime dataHora){
		int ano = dataHora.getYear();
		int mes = dataHora.getMonthValue();
		int dia = dataHora.getDayOfMonth();
		int hora = dataHora.getHour();
		int min = dataHora.getMinute();
		int seg = dataHora.getSecond();
		
		return new DateTime(ano, mes, dia, hora, min, seg);
	}
}
