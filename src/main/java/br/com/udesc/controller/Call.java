package br.com.udesc.controller;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ResourceBundle;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.maps.errors.ApiException;
import com.google.maps.model.DirectionsLeg;
import com.google.maps.model.DirectionsResult;

import br.com.udesc.converter.DataUtils;
import br.com.udesc.maps.SearchMaps;
import br.com.udesc.model.Endereco;
import br.com.udesc.model.Percurso;
import br.com.udesc.repo.EnderecoRepository;
import br.com.udesc.repo.PercursoRepository;

/**
 * @author arino.godinho 
 * 2017
 */
@RestController
public class Call {

	@Autowired
	PercursoRepository repoPercurso;

	@Autowired
	EnderecoRepository repoEndereco;

	private DirectionsResult result;
	private SearchMaps searchMaps = new SearchMaps();
	private Endereco endereco;
	private Percurso percurso;

	@RequestMapping("/*")
	public String index() {
		return "<h2>Running Application</h2>";
	}

	@RequestMapping(value = "/version", consumes = "*/*", produces = "text/plain")
	public String version() {
		ResourceBundle rb = ResourceBundle.getBundle("bundle");
		Object version = rb.getObject("version");
		Object buildNumber = rb.getObject("build_number");

		return String.join(" ", "version:", version.toString(), "\nbuild:", buildNumber.toString());
	}

	@RequestMapping(value = "/searchMaps")
	public DirectionsLeg searchMaps(
			@RequestParam(name = "origem", required = true) String origem,
			@RequestParam(name = "destino", required = true) String destino,
			@RequestParam(name = "dataInicial", required = true) String dataInicial,
			@RequestParam(name = "dataFinal", required = true) String dataFinal,
			@RequestParam(name = "hora", required = true) String hora,
			@RequestParam(name = "categoriaDistancia", required = true) int categDistancia
		) throws ApiException, InterruptedException, IOException {
		
		LocalDate dataPesquisa = DataUtils.toLocalDate(dataInicial);
		LocalDateTime horaPesquisa = DataUtils.toLocalDateTime(DataUtils.toLocalDate(dataInicial), hora);
		long dias = DataUtils.dias(DataUtils.toLocalDate(dataInicial), DataUtils.toLocalDate(dataFinal));
		
		this.endereco = new Endereco(origem, destino, categDistancia);
		this.result = this.searchMaps.request(this.endereco, DataUtils.toDateTime(horaPesquisa));

		this.endereco.setOrigem(this.result.routes[0].legs[0].startAddress);
		this.endereco.setDestino(this.result.routes[0].legs[0].endAddress);
		repoEndereco.save(this.endereco);
		
		for (int i = 0; i < dias; i++){
			this.result = this.searchMaps.request(this.endereco, DataUtils.toDateTime(horaPesquisa));
			this.percurso = new Percurso(
					this.result.routes[0].legs[0].distance.toString(),
					this.result.routes[0].legs[0].durationInTraffic.toString(), 
					dataPesquisa, 
					horaPesquisa,
					this.endereco
				);
	
			repoPercurso.save(this.percurso);
			dataPesquisa = dataPesquisa.plusDays(1);
			horaPesquisa = horaPesquisa.plusDays(1);
		}
		return this.result.routes[0].legs[0];

	}

}
