package br.com.udesc.repo;

import org.springframework.data.repository.CrudRepository;

import br.com.udesc.model.Endereco;

/**
 * @author arino.godinho 
 * 2017
 */
public interface EnderecoRepository extends CrudRepository<Endereco, Long> {

}
