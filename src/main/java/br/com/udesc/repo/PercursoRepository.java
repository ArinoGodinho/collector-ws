package br.com.udesc.repo;

import org.springframework.data.repository.CrudRepository;

import br.com.udesc.model.Percurso;

/**
 * @author arino.godinho 
 * 2017
 */
public interface PercursoRepository extends CrudRepository<Percurso, Long> {

}
