package br.com.udesc.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author arino.godinho 
 * 2017
 */
@Entity
@Table(name = "endereco")
@NoArgsConstructor
@Setter
@Getter
public class Endereco {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "endereco_id")
	private long id;

	@Column(name = "origem")
	private String origem;

	@Column(name = "destino")
	private String destino;

	@Column(name = "categoria_distancia")
	private int categoriaDistancia;

	public Endereco(String origem, String destino, int categoriaDistancia) {
		super();
		this.origem = origem;
		this.destino = destino;
		this.categoriaDistancia = categoriaDistancia;
	}

}
