package br.com.udesc.model;

import java.time.LocalDate;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

/**
 * @author arino.godinho 
 * 2017
 */
@Entity
@Table(name = "percurso")
@RequiredArgsConstructor
@NoArgsConstructor(force = true)
@Setter
@Getter
public class Percurso {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "percurso_id")
	private long id;

	@Column(name = "distancia")
	private final String distancia;

	@Column(name = "duracao")
	private final String duracao;

	@Column(name = "data")
	private final LocalDate data;

	@Column(name = "hora")
	private final LocalDateTime hora;

	@ManyToOne
	@JoinColumn(name = "endereco_id", nullable = false)
	private final Endereco enderecoPercurso;

}
