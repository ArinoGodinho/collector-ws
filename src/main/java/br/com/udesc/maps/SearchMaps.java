package br.com.udesc.maps;

import java.io.IOException;

import org.apache.log4j.Level;
import org.joda.time.DateTime;

import com.google.maps.DirectionsApi;
import com.google.maps.GeoApiContext;
import com.google.maps.errors.ApiException;
import com.google.maps.model.DirectionsResult;
import com.google.maps.model.TrafficModel;
import com.google.maps.model.TravelMode;
import com.google.maps.model.Unit;

import br.com.udesc.model.Endereco;
import lombok.extern.log4j.Log4j;

/**
 * @author arino.godinho 
 * 2017
 */
@Log4j
public class SearchMaps {

	private GeoApiContext context;
	private DirectionsResult result;

	public SearchMaps() {
		this.context = ContextMaps.getInstance().getContext().setQueryRateLimit(7);
	}

	public DirectionsResult request(Endereco endereco, DateTime dataHoraInSeconds){
		try {
			this.result = DirectionsApi.newRequest(this.context)
					.origin(endereco.getOrigem())
					.destination(endereco.getDestino())
					.language("pt_BR")
					.mode(TravelMode.DRIVING)
					.units(Unit.METRIC)
					.trafficModel(TrafficModel.BEST_GUESS)
					.departureTime(dataHoraInSeconds).await();
		} catch (ApiException | InterruptedException | IOException e) {
			log.setLevel(Level.ERROR);
			log.error(this.result);
			e.printStackTrace();
		}

		return this.result;
	}
}
