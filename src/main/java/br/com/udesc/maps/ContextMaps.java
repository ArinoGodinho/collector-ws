package br.com.udesc.maps;

import com.google.maps.GeoApiContext;

/**
 * @author arino.godinho 
 * 2017
 */
public class ContextMaps {

	private static ContextMaps instance;
	private GeoApiContext context;

	private ContextMaps() {
		this.context = new GeoApiContext().setApiKey("AIzaSyDelRzchIifqh4ye5LqjrR5fDF8fPe4Iqk");
	}

	public static synchronized ContextMaps getInstance() {
		if (instance == null) {
			instance = new ContextMaps();
		}
		return instance;

	}

	public GeoApiContext getContext() {
		return this.context;
	}
}
